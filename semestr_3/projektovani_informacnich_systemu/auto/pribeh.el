(TeX-add-style-hook
 "pribeh"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("babel" "czech")))
   (TeX-run-style-hooks
    "latex2e"
    "book"
    "bk10"
    "inputenc"
    "fontenc"
    "babel"
    "mathtools"
    "amsmath"))
 :latex)

