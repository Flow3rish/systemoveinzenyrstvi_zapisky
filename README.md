# Základní informace

Zápisky s příponou .tex jsou psané v LaTeX markupu, slouží pro editování. Pro pohodlnější čtení je doporučeno si otevřít soubor s příponou .pdf (zkompilovaný dokument).

Soubor template.tex slouží jako šablona pro budoucí zápisky.

# V případě nalezených chyb je možno

* Kontaktovat autora
* opravit soubor, udělat commit a vytvořit merge request

# Přispívání

Přispívání je možné a velmi vítané.
